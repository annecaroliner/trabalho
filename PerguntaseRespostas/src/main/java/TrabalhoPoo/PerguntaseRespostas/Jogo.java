/*
 * 10 perguntas V
 * 1 pergunta com 4 opções de respostas
 * 1 imagem associada
 * Taxa de acerto apos terminar o jogo
 * historico de partidas com resultados e data de encerramento
 * criar txt com historico*/


package TrabalhoPoo.PerguntaseRespostas;
import javax.swing.*;

public class Jogo {
	    public static void main(String[] args) {
	        int Pontos = 0;
	        String usuario;
	        String menu;
	        
	        
	        usuario = JOptionPane.showInputDialog("Digite seu nome: ");
	        JOptionPane.showMessageDialog(null, "Seja bem vindo(a) " + usuario);
	        String fase1, fase2, fase3;
	        
	        /* Pergunta 1 */
	        fase1 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        + "O que significa if? \n"
	        + "A) Então \n"
	        + "B) Talvez \n"
	        + "C) Ok \n"
	        + "D) Se");
	        
	        if (("D".equals(fase1) || ("d".equals(fase1)))){
	            JOptionPane.showMessageDialog(null,"Resposta correta!");
	            Pontos=Pontos+1000;
	        }
	        else {
	            JOptionPane.showMessageDialog(null,"Resposta incorreta");
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 2 */
	        fase1 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	                + "O que significa Else? \n"
	                + "A) Se \n"
	                + "B) Se não \n"
	                + "C) Ok \n"
	                + "D) Não sei \n");
	        
	        if("B".equals(fase1) || ("b".equals(fase1))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+1000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 3 */
	        fase1 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "Para que serve o Import Java.sql.*;? \n"
	        		+ "A) Conexão com o mundo \n"
	        		+ "B) Conexão com a classe \n"
	        		+ "C) Conexão com o banco de dados \n"
	        		+ "D) Conexão com o package");
	        
	        if("C".equals(fase1) || ("c".equals(fase1))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+1000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 4 */
	        fase1 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "Para que serve JOptionPane? \n"
	        		+ "A) Para criar uma caixa de dialógo \n"
	        		+ "B) Fazer pesquisas \n"
	        		+ "C) Mudar fonte da letra \n"
	        		+ "D) Digitar alguma mensagem");
	        
	        if("A".equals(fase1) || ("a".equals(fase1))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+1000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 5 */
	        fase1 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "O que é o metodo main? \n"
	        		+ "A) É uma caixa de dialógo \n"
	        		+ "B) É um metodo para fazer pesquisas \n"
	        		+ "C) É metodo que criptografa os textos \n"
	        		+ "D) É o metodo principal");
	        
	        if("D".equals(fase1) || ("d".equals(fase1))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+1000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        JOptionPane.showMessageDialog(null,"PARABÉNS! VOCÊ CHEGOU A SEGUNDA FASE");
	        
	        /* Pergunta 6 */
	        fase2 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "O que é java? \n"
	        		+ "A) É uma caixa de dialógo \n"
	        		+ "B) É um metodo para fazer pesquisas \n"
	        		+ "C) É uma linguagem de programação orientada a objetos \n"
	        		+ "D) É o metodo principal");
	        
	        if("C".equals(fase2) || ("c".equals(fase2))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+5000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 7 */
	        fase2 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "Quantas pessoas faziam parte do time de construção do java? \n"
	        		+ "A) 12 pessoas \n"
	        		+ "B) 13 pessoas \n"
	        		+ "C) 15 pessoas \n"
	        		+ "D) 25 pessoas");
	        
	        if("B".equals(fase2) || ("b".equals(fase2))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+5000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 8 */
	        fase2 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "Quem foi o lider do projeto java? \n"
	        		+ "A) James Gosling \n"
	        		+ "B) Albert Einstein \n"
	        		+ "C) Platão \n"
	        		+ "D) Isaac Newton");
	        
	        if("A".equals(fase2) || ("a".equals(fase2))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+5000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 9 */
	        fase2 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "Onde é utilizada a linguagem java? \n"
	        		+ "A) É utilizada para desenvolvimento de aplicações desktop \n"
	        		+ "B) É utilizado para fazer pesquisas \n"
	        		+ "C) É utilizado para criptografar textos \n"
	        		+ "D) É utilizada para criar telas principais");
	        
	        if("A".equals(fase2) || ("a".equals(fase2))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+5000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        /* Pergunta 10 */
	        fase2 = JOptionPane.showInputDialog("Usuario: " +usuario + "\nPontos: " + Pontos+"\n"
	        		+ "Qual o componente da plataforma java assegura a independencia das aplicações entre diferentes plataformas? \n"
	        		+ "A) Java Aplication \n"
	        		+ "B) Java Virtual Machine \n"
	        		+ "C) Java ER \n"
	        		+ "D) Enterprise Edition");
	        
	        if("B".equals(fase2) || ("b".equals(fase2))){
	            JOptionPane.showMessageDialog(null, "Resposta correta!");
	            Pontos=Pontos+5000;
	        }
	        else {
	        	JOptionPane.showMessageDialog(null,"Resposta incorreta");
	        	Pontos = Pontos/2;
	            JOptionPane.showMessageDialog(null,"A sua pontuação foi: " + Pontos);
	            System.exit(0);
	        }
	        
	        JOptionPane.showMessageDialog(null,"PARABÉNS! VOCÊ CHEGOU AO FIM DO QUIZ COM " + Pontos + "\nPONTOS");
	    }
	    
	}